# FrozenDNS
nameserver 92.222.97.144
nameserver 92.222.97.145
nameserver 192.99.85.244

# ParrotDNS/OpenNIC
nameserver 139.99.96.146
nameserver 37.59.40.15
nameserver 185.121.177.177


# OpenDNS
nameserver 208.67.222.222
nameserver 208.67.220.220


# OpenNIC
nameserver 185.121.177.177

# Mullvad
nameserver 193.138.219.228

## =================================================================== ##
##	Add these ip addresses to network managers connections editor	   ##
## 			Under ipv4 settings, add this line to DNS Servers		   ##
## =================================================================== ##
92.22.97.145,185.121.177.177,192.99.85.244,208.67.222.222,208.67.220.220






# Preserve the existing resolv.conf
  if [ -e /etc/resolv.conf ] ; then
    cp /etc/resolv.conf /etc/resolv.conf.ovpnsave
  fi
  printf "%s\n" "${out}" > /etc/resolv.conf
  chmod 644 /etc/resolv.conf





