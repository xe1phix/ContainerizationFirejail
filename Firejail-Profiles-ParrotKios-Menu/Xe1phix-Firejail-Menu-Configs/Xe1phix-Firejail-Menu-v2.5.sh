Engrampa Archive Manager
engrampa %U
engrampa 
Create and modify an archive

/usr/share/icons/maia/apps/scalable/engrampa.svg

atril 
xpdf

peepdf
pdf-parser
pdfid


rtlsdr-scanner
rfcat
hackrf_info


btscanner
spooftooph


mate-panel
galculator
eom 

gdebi-gtk 	


Geany {Firejail Sandboxed}
firejail --profile=/etc/firejail/geany.profile --net=none /usr/bin/geany

xpdf {Firejail Sandboxed}
firejail --profile=/etc/firejail/xpdf.profile --net=none /usr/bin/xpdf %f

Evince {Firejail Sandboxed}
firejail --profile=/etc/firejail/evince.profile --net=none /usr/bin/evince

Atril Document Viewer {Firejail Sandboxed}
firejail --profile=/etc/firejail/atril.profile --net=none /usr/bin/atril %U



firejail --profile=/etc/firejail/vlc.profile /usr/bin/vlc --started-from-file %U


HexChat {Firejail Sandboxed}
firejail --profile=/etc/firejail/hexchat.profile /usr/bin/hexchat

Firefox-esr {Firejail Sandboxed}
firejail --profile=/etc/firejail/firefox-common.profile /usr/bin/firefox-esr


Audacious {Firejail Sandboxed}
firejail --profile=/etc/firejail/audacious.profile --net=none audacious



mpv Media Player {Firejail Sandboxed}
firejail --caps.drop=all --net=none --seccomp --nonewprivs --private-tmp --shell=none  /usr/bin/mpv --player-operation-mode=pseudo-gui

Totem {Firejail Sandboxed}
firejail --profile=/etc/firejail/totem.profile /usr/bin/totem %U

Gnash SWF Viewer {Firejail Sandboxed}
firejail --profile=/etc/firejail/gnash.profile /usr/bin/gnash-gtk-launcher %U

MediaInfo-gtk {Firejail Sandboxed}
firejail --profile=/etc/firejail/mediainfo.profile /usr/bin/mediainfo-gui %f

OpenShot Video Editor {Firejail Sandboxed}
openshot %F



Pluma Text Editor {Firejail Sandboxed}
firejail --profile=/etc/firejail/pluma.profile /usr/bin/pluma %U

Zim Desktop Wiki {Firejail Sandboxed}
firejail --profile=/etc/firejail/zim.profile /usr/bin/zim %f


Archive Manager {Firejail Sandboxed}
firejail --profile=/etc/firejail/file-roller.profile /usr/bin/file-roller %U


Engrampa Archive Manager {Firejail Sandboxed}
firejail --profile=/etc/firejail/engrampa.profile /usr/bin/engrampa %U



Liferea {Firejail Sandboxed}
firejail --profile=/etc/firejail/liferea.profile /usr/bin/liferea %U

Thunderbird {Firejail Sandboxed}
firejail --profile=/etc/firejail/thunderbird.profile /usr/bin/thunderbird %u

qBittorrent {Firejail Sandboxed}
firejail --profile=/etc/firejail/qbittorrent.profile /usr/bin/qbittorrent %U


Transmission-gtk {Firejail Sandboxed}
firejail --profile=/etc/firejail/transmission-gtk.profile /usr/bin/transmission-gtk %U

Eye of MATE Image Viewer {Firejail Sandboxed}
firejail --profile=/etc/firejail/eom.profile /usr/bin/eom %U


Eye of Gnome Image Viewer {Firejail Sandboxed}
firejail --profile=/etc/firejail/eog.profile /usr/bin/eog %U


Metadata Anonymization Toolkit (MAT) {Firejail Sandboxed}
firejail --profile=/etc/firejail/mat.profile /usr/bin/mat-gui

Caja File Browser {Firejail Sandboxed}
firejail --profile=/etc/firejail/caja.profile /usr/bin/caja --no-desktop --browser %U

firejail --profile=/etc/firejail/seahorse.profile /usr/bin/seahorse

firejail --profile=/etc/firejail/.profile /usr/bin/
firejail --profile=/etc/firejail/.profile /usr/bin/
firejail --profile=/etc/firejail/.profile /usr/bin/

gnunet-fs-gtk

gnunet-setup-pkexec




gpa 

hexchat --existing 


owasp-mantra-ff
zaproxy

p0f



hping3


htop





msfconsole-start
msfstart
 msfstop
msfpc
nbtscan
msfupdate 
setoolkit



bully
recon-ng
netsniff-ng 
nmap
openvas-check-setup
openvas-feed-update
gsd
openvas-setup
xhydra 
zenmap 
wireshark.profile 
/usr/bin/etherape
ettercap -G
evilgrade
wifite
pixiewps
 /usr/bin/penmode2
fping
giskismet
us 
golismero


ewfacquire
ewfexport
ewfinfo
ewfverify
istat
vol -h
reaver
volafox
srch_strings 

unix-privesc-check
/usr/sbin/tiger

fsstat

transmission-gtk 


/usr/bin/gnome-keyring-daemon --start --components=secrets


torbrowser-launcher 
torbrowser-launcher --settings
/usr/bin/torchat
proxychains

Kernel packet filtering and screening whitepaper.pdf

VirtualBox 
/usr/bin/VirtualBox

qemu-launcher

send the metadata information, without any data blocks. 
e2image -r /dev/hda1 - | bzip2 > hda1.e2i.bz2

Kernel Boot Command-Line Parameter Reference


create a QCOW2 image file 
e2image -Q /dev/hda1 hda1.qcow2
bzip2 -z hda1.qcow2
A QCOW2 image contains all the information the raw image does, however unlike the raw image it is not sparse.



clone a fs into an image file
e2image -arO 1048576 /dev/sda1 img


dd image to ext2 to sda
e2image -aro 1048576 img /dev/sda1





convert a qcow2 image into a raw image
e2image -r hda1.qcow2 hda1.raw




/usr/bin/vlc --started-from-file 

Secret Storage Service
/usr/bin/gnome-keyring-daemon --start --components=secrets
GNOME Keyring: Secret Service



Certificate and Key Storage
/usr/bin/gnome-keyring-daemon --start --components=pkcs11
GNOME Keyring: PKCS#11 Component


PolicyKit Authentication Agent
/usr/lib/x86_64-linux-gnu/polkit-mate-authentication-agent-1
PolicyKit Authentication Agent for the MATE Desktop

DNSSEC Trigger
/usr/bin/dnssec-trigger-panel
Shows DNS state and warning dialog



MATE NVIDIA Optimus
/usr/lib/mate-optimus/mate-optimus-applet
Shows a tray icon when a compatible NVIDIA Optimus graphics card is detected


MATE Tweak
mate-tweak


Network
nm-applet
Manage your network connections


MATE Settings Daemon
/usr/bin/mate-settings-daemon
ssh-agent


AT-SPI D-Bus Bus
/usr/lib/at-spi2-core/at-spi-bus-launcher --launch-immediately


AppArmor Notify
/usr/bin/aa-notify -p -s 1 -w 60	
Receive on screen notifications of AppArmor denials

Adobe Flash Player
flash-player-properties
Preferences for Adobe Flash Player



PulseAudio Sound System
start-pulseaudio-x11

Start the PulseAudio Sound System


Volume Control
mate-volume-control-applet
Show desktop volume control


SSH Key Agent
/usr/bin/gnome-keyring-daemon --start --components=ssh
GNOME Keyring: SSH Agent


Blueman Applet
blueman-applet
Blueman Bluetooth Manager





mc
mate-terminal
mate-system-monitor
xterm
uxterm
gksu /usr/bin/x-terminal-emulator




rkhunter


sandi-gu
intrace



xpra_launcher 
xpra attach 


palimpsest

gksu tccf-gui
tccf-gui

gksu /usr/bin/pluma
gksu /usr/bin/caja
abiword 
/opt/atom/atom 



dnsrecon
dnsmap
dnsspoof
dsniff












socat


anonsurf starti2p
anonsurf stopi2p
anonsurf change



service ssh start 
service ssh stop 

ssvnc -noenc


/usr/lib/notification-daemon/notification-daemon
gksu mate-system-log

xlinks2
kismet


pluma 
keepnote 


seahorse-tool --import

seahorse-tool --decrypt
seahorse-tool --verify

policygen
/usr/bin/seahorse




/usr/share/icons/maia/apps/scalable/eom.svg














FireJailed Atril Document Viewer
atril %U
firejail --profile=/home/faggot/Firejail/Profiles/etc/atril.profile atril %U
View multi-page documents in firejailed secure environment



Document Viewer
firejail --profile=/etc/firejail/evince.profile
evince %U
View multi-page documents
/usr/share/icons/maia/apps/scalable/evince.svg
/usr/share/icons/maia/apps/scalable/epdfview.svg

/usr/bin/firejail --profile=/home/faggot/Firejail/Profiles/etc/atril.profile /usr/bin/atril %U

firejail --profile=/etc/firejail/


firejail --profile=/etc/firejail/disable-common.inc
firejail --profile=/etc/firejail/disable-devel.inc
firejail --profile=/etc/firejail/disable-passwdmgr.inc
firejail --profile=/etc/firejail/disable-programs.inc
firejail --profile=/etc/firejail/display.profile
firejail --profile=/etc/firejail/whitelist-common.inc


firejail --profile=/etc/firejail/default.profile
firejail --profile=/etc/firejail/firejail.config
firejail --profile=/etc/firejail/firejail-default




firejail --profile=/etc/firejail/evince-previewer 

firejail --profile=/etc/firejail/login.users


firejail --profile=/etc/firejail/xpra.profile
firejail --profile=/etc/firejail/virtualbox.profile
firejail --profile=/etc/firejail/qemu-system-x86_64.profile
firejail --profile=/etc/firejail/qemu-launcher.profile
firejail --profile=/etc/firejail/dosbox.profile


firejail --profile=/etc/firejail/unbound.profile

firejail --profile=/etc/firejail/transmission-show.profile
firejail --profile=/etc/firejail/transmission-qt.profile
firejail --profile=/etc/firejail/transmission-cli.profile


Transmission

firejail --name=transmission --profile=/etc/firejail/transmission-gtk.profile /usr/bin/transmission-gtk %U

--read-write=~/Downloads/

firejail --debug-blacklists 
firejail --debug-caps
firejail --debug-check-filename firefox
firejail --debug-errnos
firejail --debug-protocols
firejail --debug-syscalls
firejail --debug-whitelists 
firejail --caps.print=
firejail --seccomp.print=

firejail --trace wget -q www.debian.org
firejail --tracelog 
firejail --audit transmission-gtk
firejail --audit=~/sandbox-test transmission-gtk

 --mac=00:11:22:33:44:55 
firejail --net=br0 --net=br1
firejail --net=br0 --veth-name=
firejail --net=eth0 --ip=192.168.1.80 --dns=8.8.8.8 

--whitelist=~/


private-bin=transmission-gtk
Download and share files over BitTorrent
/usr/share/icons/maia/apps/scalable/transmission.svg

firejail --profile=/etc/firejail/rtorrent.profile
firejail --profile=/etc/firejail/qbittorrent.profile
firejail --profile=/etc/firejail/deluge.profile



firejail --profile=/etc/firejail/emacs.profile
firejail --profile=/etc/firejail/vim.profile

firejail --profile=/etc/firejail/atom-beta.profile
firejail --profile=/etc/firejail/atom.profile
firejail --profile=/etc/firejail/kate.profile
firejail --profile=/etc/firejail/libreoffice.profile


firejail --profile=/etc/firejail/mediainfo.profile
firejail --profile=/etc/firejail/file.profile

firejail --profile=/etc/firejail/dolphin.profile
firejail --profile=/etc/firejail/nautilus.profile



Caja {SuperUser}
gksu /usr/bin/caja
Browse the file system with the file manager
/usr/share/icons/hicolor/scalable/apps/caja.svg


Caja {Firejail Sandboxed}
firejail --name=caja --profile=/etc/firejail/caja.profile --net=none /usr/bin/caja --no-desktop --browser %U
Browse the file system with the file manager
/usr/share/icons/maia/apps/scalable/system-file-manager.svg




Pluma Text Editor {Firejail Sandboxed}
firejail --name=pluma --profile=/etc/firejail/pluma.profile --net=none /usr/bin/pluma %U
Edit text files
/usr/share/icons/maia/apps/scalable/gedit-logo.svg

Pluma {SuperUser}
gksu --sudo-mode /usr/bin/pluma
Edit text files
/usr/share/icons/maia/apps/scalable/gedit-logo.svg


Pluma Text Editor {Firejail Private Sandbox}
firejail --name=pluma --profile=/etc/firejail/pluma.profile --private --private-cache /usr/bin/pluma %U
Browse the file system with the file manager
/usr/share/icons/maia/apps/scalable/system-file-manager.svg



firejail --profile=/etc/firejail/gedit.profile
/usr/share/icons/maia/apps/scalable/gedit-logo.svg

zenmap
/usr/share/icons/maia/apps/scalable/activity-log-manager.svg



ufw
/usr/share/icons/maia/apps/scalable/AdobeFirework.svg


/usr/share/icons/maia/apps/scalable/AdobeUpdate.svg




/usr/share/icons/maia/apps/scalable/android-studio.svg



firejail --profile=/etc/firejail/keepass.profile
firejail --profile=/etc/firejail/keepass2.profile
firejail --profile=/etc/firejail/gpa.profile

firejail --profile=/etc/firejail/git.profile

firejail --profile=/etc/firejail/gpg-agent.profile
firejail --profile=/etc/firejail/gpg.profile
firejail --profile=/etc/firejail/ssh.profile
firejail --profile=/etc/firejail/ssh-agent.profile


firejail --profile=/etc/firejail/gthumb.profile
firejail --profile=/etc/firejail/mupdf.profile
firejail --profile=/etc/firejail/okular.profile
firejail --profile=/etc/firejail/pdftotext.profile
firejail --profile=/etc/firejail/xpdf.profile
firejail --profile=/etc/firejail/qpdfview.profile


firejail --profile=/etc/firejail/brasero.profile
firejail --profile=/etc/firejail/audacity.profile
firejail --profile=/etc/firejail/audacious.profile
firejail --profile=/etc/firejail/gnome-mplayer.profile
firejail --profile=/etc/firejail/xplayer.profile





VLC Media Player {Firejail}
/usr/bin/firejail --name=vlc --profile=/etc/firejail/vlc.profile --net=none /usr/bin/vlc --started-from-file %U
Read, capture, broadcast your multimedia streams
/usr/share/icons/maia/apps/scalable/vlc.svg



firejail --profile=/etc/firejail/totem.profile
firejail --profile=/etc/firejail/amarok.profile




firejail --profile=/etc/firejail/telegram.profile
firejail --profile=/etc/firejail/Telegram.profile



firejail --profile=/etc/firejail/hexchat.profile
firejail --profile=/etc/firejail/pidgin.profile
firejail --profile=/etc/firejail/xchat.profile
firejail --profile=/etc/firejail/wire.profile
firejail --profile=/etc/firejail/cryptocat.profile
firejail --profile=/etc/firejail/Cryptocat.profile
firejail --profile=/etc/firejail/weechat.profile
firejail --profile=/etc/firejail/weechat-curses.profile
firejail --profile=/etc/firejail/thunderbird.profile
firejail --profile=/etc/firejail/claws-mail.profile
firejail --profile=/etc/firejail/clementine.profile
firejail --profile=/etc/firejail/jitsi.profile
firejail --profile=/etc/firejail/kmail.profile
firejail --profile=/etc/firejail/konversation.profile
firejail --profile=/etc/firejail/mutt.profile


firejail --profile=/etc/firejail/strings.profile


firejail --profile=/etc/firejail/xreader.profile
firejail --profile=/etc/firejail/xviewer.profile


firejail --profile=/etc/firejail/eom.profile
firejail --caps.drop=all --net=none --seccomp --private oem %f



firejail --name=eom --profile=/etc/firejail/eom.profile --net=none /usr/bin/eom --new-instance 


firejail --profile=/etc/firejail/eog.profile

firejail --profile=/etc/firejail/dropbox.profile



--read-only=
firejail --read-only=~/.mozilla 
firejail --protocol.print=

firejail --profile=/etc/firejail/dnsmasq.profile
firejail --profile=/etc/firejail/dnscrypt-proxy.profile


firejail --profile=/etc/firejail/google-chrome-beta.profile
firejail --profile=/etc/firejail/google-chrome.profile
firejail --profile=/etc/firejail/google-chrome-stable.profile
firejail --profile=/etc/firejail/google-chrome-unstable.profile
firejail --profile=/etc/firejail/chromium.profile
firejail --profile=/etc/firejail/chromium-browser.profile

firejail --profile=/etc/firejail/cyberfox.profile
firejail --profile=/etc/firejail/Cyberfox.profile
firejail --profile=/etc/firejail/firefox-esr.profile
firejail --profile=/etc/firejail/iceweasel.profile
firejail --profile=/etc/firejail/icecat.profile
firejail --profile=/etc/firejail/icedove.profile



firejail --private --apparmor --seccomp --name=firefox  --profile=/etc/firejail/firefox.profile --overlay-tmpfs --dns=92.222.97.144 --dns=208.67.222.222  /usr/lib/firefox-esr/firefox-esr %u



/usr/lib/firefox-esr/firefox-esr -new-tab https://boards.4chan.org/b/

/usr/share/owasp-mantra-ff/Mantra/firefox -new-tab https://boards.4chan.org/b/ -no-remote

firejail --profile=/etc/firejail/lynx.profile
firejail --profile=/etc/firejail/start-tor-browser.profile
firejail --profile=/etc/firejail/abrowser.profile
firejail --profile=/etc/firejail/wget.profile
firejail --profile=/etc/firejail/opera-beta.profile
firejail --profile=/etc/firejail/opera.profile


firejail --profile=/etc/firejail/aweather.profile
firejail --profile=/etc/firejail/bleachbit.profile

firejail --profile=/etc/firejail/exiftool.profile


firejail --profile=/etc/firejail/file-roller.profile
firejail --profile=/etc/firejail/gzip.profile
firejail --profile=/etc/firejail/xz.profile
firejail --profile=/etc/firejail/unzip.profile
firejail --profile=/etc/firejail/unrar.profile
firejail --profile=/etc/firejail/tar.profile
firejail --profile=/etc/firejail/7z.profile
firejail --profile=/etc/firejail/cpio.profile


firejail --profile=/etc/firejail/lxterminal.profile


mate-terminal


firejail --profile=/etc/firejail/




firejail --profile=/etc/firejail/








firejail --profile=/etc/firejail/chromium-browser.profile
firejail --profile=/etc/firejail/chromium-dev.profile
firejail --profile=/etc/firejail/chromium.profile





firejail --profile=/etc/firejail/deluge.profile
firejail --profile=/etc/firejail/rtorrent.profile
firejail --profile=/etc/firejail/transmission-cli.profile
firejail --profile=/etc/firejail/transmission-gtk.profile
firejail --profile=/etc/firejail/transmission-qt.profile
firejail --profile=/etc/firejail/qbittorrent.profile


firejail --profile=/etc/firejail/spotify.profile

firejail --profile=/etc/firejail/ssh.profile



firejail --profile=/etc/firejail/teamspeak3.profile
firejail --profile=/etc/firejail/steam-native.profile
firejail --profile=/etc/firejail/steam.profile
firejail --profile=/etc/firejail/skypeforlinux.profile
firejail --profile=/etc/firejail/skype.profile



firejail --profile=/etc/firejail/seamonkey-bin.profile
firejail --profile=/etc/firejail/seamonkey.profile


firejail --profile=/etc/firejail/server.profile
firejail --profile=/etc/firejail/shotcut.profile







firejail --profile=/etc/firejail/gthumb.profile
firejail --profile=/etc/firejail/gwenview.profile
firejail --profile=/etc/firejail/evolution.profile
firejail --profile=/etc/firejail/okular.profile
firejail --profile=/etc/firejail/openshot.profile
firejail --profile=/etc/firejail/gimp.profile


firejail --profile=/etc/firejail/keepass.profile
firejail --profile=/etc/firejail/keepassx2.profile
firejail --profile=/etc/firejail/keepassx.profile


firejail --profile=/etc/firejail/linphone.profile
firejail --profile=/etc/firejail/kmail.profile
firejail --profile=/etc/firejail/konversation.profile
firejail --profile=/etc/firejail/jitsi.profile
firejail --profile=/etc/firejail/hexchat.profile
firejail --profile=/etc/firejail/ricochet.profile
firejail --profile=/etc/firejail/pidgin.profile
firejail --profile=/etc/firejail/thunderbird.profile
firejail --profile=/etc/firejail/Telegram.profile
firejail --profile=/etc/firejail/claws-mail.profile





firejail --profile=/etc/firejail/audacious.profile
firejail --profile=/etc/firejail/audacity.profile
firejail --profile=/etc/firejail/lmms.profile
firejail --profile=/etc/firejail/rhythmbox.profile
firejail --profile=/etc/firejail/totem.profile


firejail --profile=/etc/firejail/tar.profile
firejail --profile=/etc/firejail/generic-compression.profile
firejail --profile=/etc/firejail/gzip.profile
firejail --profile=/etc/firejail/gtar.profile
firejail --profile=/etc/firejail/7z.profile







firejail --profile=/etc/firejail/abrowser.profile
firejail --profile=/etc/firejail/icecat-launcher.profile
firejail --profile=/etc/firejail/icecat.profile
firejail --profile=/etc/firejail/icedove.profile
firejail --profile=/etc/firejail/iceweasel.profile
firejail --profile=/etc/firejail/torbrowser-launcher.profile
firejail --profile=/etc/firejail/start-tor-browser.profile
firejail --profile=/etc/firejail/opera-beta.profile
firejail --profile=/etc/firejail/opera.profile
firejail --profile=/etc/firejail/firefox-esr.profile
firejail --profile=/etc/firejail/firefox.profile

firejail --profile=/etc/firejail/chromium-browser.profile
firejail --profile=/etc/firejail/chromium-dev.profile
firejail --profile=/etc/firejail/chromium.profile
firejail --profile=/etc/firejail/google-chrome-beta.profile
firejail --profile=/etc/firejail/google-chrome.profile
firejail --profile=/etc/firejail/google-chrome-stable.profile
firejail --profile=/etc/firejail/google-chrome-unstable.profile

firejail --profile=/etc/firejail/google-earth.profile



firejail --profile=/etc/firejail/gnome-clocks.profile
firejail --profile=/etc/firejail/gnome-contacts.profile
firejail --profile=/etc/firejail/gnome-maps.profile
firejail --profile=/etc/firejail/gnome-mplayer.profile
firejail --profile=/etc/firejail/gnome-music.profile
firejail --profile=/etc/firejail/gnome-weather.profile
firejail --profile=/etc/firejail/gnome-2048.profile
firejail --profile=/etc/firejail/gnome-calculator.profile
firejail --profile=/etc/firejail/gnome-calendar.profile
firejail --profile=/etc/firejail/aweather.profile



firejail --profile=/etc/firejail/git.profile
firejail --profile=/etc/firejail/gitter.profile


firejail --profile=/etc/firejail/dnscrypt-proxy.profile
firejail --profile=/etc/firejail/dnsmasq.profile

firejail --profile=/etc/firejail/dosbox.profile
firejail --profile=/etc/firejail/dropbox.profile
firejail --profile=/etc/firejail/empathy.profile


firejail --profile=/etc/firejail/eog.profile
firejail --profile=/etc/firejail/eom.profile
firejail --profile=/etc/firejail/file.profile
firejail --profile=/etc/firejail/less.profile




pl
firejail --profile=/etc/firejail/defaultnd.profile
firejail --profile=/etc/firejail/default.profile
firejail --profile=/etc/firejail/defaultw.profile





firejail --profile=/etc/firejail/claws-mail.profile




firejail --profile=/etc/firejail/atom-beta.profile
firejail --profile=/etc/firejail/atom.profile

firejail --profile=/etc/firejail/qpdfview.profile
firejail --profile=/etc/firejail/atril.profile
firejail --profile=/etc/firejail/epiphany.profile
firejail --profile=/etc/firejail/evince.profile








firejail --profile=/etc/firejail/filezilla.profile


firejail --profile=/etc/firejail/firejail.config
firejail --profile=/etc/firejail/disable-common.inc
firejail --profile=/etc/firejail/disable-devel.inc
firejail --profile=/etc/firejail/disable-passwdmgr.inc
firejail --profile=/etc/firejail/disable-programs.inc
firejail --profile=/etc/firejail/default.profile
firejail --profile=/etc/firejail/disable-programs.inc
firejail --profile=/etc/firejail/display.profile









