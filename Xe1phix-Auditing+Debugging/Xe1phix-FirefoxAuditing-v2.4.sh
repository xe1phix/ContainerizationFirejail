#!/bin/sh
## Xe1phix-FirefoxAuditing-v2.4.sh



## Firejail Telegram Auditing {No Profile Audit}
firejail --audit --name=firefox --noprofile /usr/bin/firefox-esr %u

firejail --noprofile --output=~/Firefox-debug.txt --debug /usr/bin/firefox-esr %u

## Firefox-esr - Enhanced Audit - {Tracing, Debugging, Auditing, and strace}
firejail --trace --debug --audit --noprofile /usr/bin/firefox-esr

firejail --trace --debug --audit --noprofile strace -f /usr/bin/firefox-esr


firejail --trace firefox-esr


firejail --debug-protocols --noprofile firefox-esr



firejail --audit --noprofile /usr/bin/firefox-esr

--allow-debuggers
firejail --allow-debuggers --noprofile strace -f firefox-esr

## Firefox Debugging {Firejail Strace} 
firejail --allow-debuggers --profile=/etc/firejail/firefox-esr.profile strace -f /usr/bin/firefox-esr %u
firejail --allow-debuggers --noprofile strace -f /usr/bin/firefox-esr %u

## Firefox Debugging {Firejail Print Seccomp Syscalls} 
firejail --seccomp.print=firefox

firejail --tracelog /usr/bin/firefox-esr %u
tail -f /var/log/syslog

firejail --trace wget -q www.debian.org

## Firefox Debugging {Firejail Debug Blacklists} 
firejail --debug-blacklists /usr/bin/firefox-esr %u
firejail --noprofile --debug-blacklists /usr/bin/firefox-esr %u

## Firefox Debugging {Firejail Debug Capabilities} 
firejail --debug-caps /usr/bin/firefox-esr
firejail --noprofile --debug-caps /usr/bin/firefox-esr %u

## Firefox Debugging {Firejail Debug Protocols} 
firejail --debug-protocols firefox-esr

## Firefox Debugging {Firejail Debug Syscalls} 
firejail --debug-syscalls /usr/bin/firefox-esr
firejail --noprofile --debug-syscalls /usr/bin/firefox-esr %u

## Firefox Debugging {Firejail Debug Whitelists} 
firejail --debug-whitelists /usr/bin/firefox-esr
firejail --noprofile --debug-whitelists /usr/bin/firefox-esr %u



firejail --join-network=firefox /sbin/iptables -vL
firejail --join-network=firefox ip addr

--netfilter.print=
--netfilter6.print=

--dns.print=



## Firejail Firefox Debugging {Print Protocols} 
firejail --name=firefox --profile=/etc/firejail/firefox-common.profile /usr/bin/firefox-esr %u
firejail --protocol.print=firefox --output=firefox-protocols.txt



## Firejail Firefox Debugging {Firejail Print Seccomp Syscalls} 
(firejail --name=firefox --noprofile --output=~/FirejailFirefoxSeccompPrint.txt /usr/bin/firefox-esr) && sleep 10 && firejail --seccomp.print=firefox  | tee ~/FirejailSeccompFirefoxProfile.txt


## Firejail Firefox Debugging {Firejail Print all recognized errors} 
firejail --debug-errnos /usr/bin/firefox-esr %u

## Firejail Firefox Logging {Cp stdout & stderr to logfile}
firejail --name=firefox --output=~/FirefoxLogz --profile=/etc/firejail/firefox-common.profile /usr/bin/firefox-esr %u



echo "Listing Log Files..." 
ls -l sandboxlog* && cat sandboxlog* | tee ~/sandboxlog.txt


## Firejail Telegram Auditing {No Profile Audit}
firejail --audit --name=firefox --noprofile /usr/bin/firefox-esr %u


## Firejail Telegram Auditing {Profile Defined Audit}
firejail --audit --name=firefox --profile=/etc/firejail/firefox-common.profile

firejail --ls=firefox ~/Downloads
firejail --dns.print=
firejail --caps.print=
firejail --fs.print=
firejail --protocol.print=firefox
firejail --seccomp.print=firefox --output=FirefoxLogz
--apparmor.print=
firejail --tree
firejail --netstats
firejail --top
firejail --debug-check-filename firefox


firejail --debug --join=firefox
cat /proc/self/status | grep Cap

## Audit/Debug without the predefined security profiles in /etc/firejail/.
firejail --noprofile --output=~/Firefox-debug.txt --debug 

firejail --noprofile --output=~/Firefox-debug.txt --debug /usr/bin/firefox-esr %u

firejail --caps /etc/init.d/bluetooth start


firejail --output-stderr=
firejail --output=sandboxlog
firejail --profile.print=firefox

firejail --netfilter.print=
firejail --netfilter6.print=



