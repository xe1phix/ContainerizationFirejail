# Firejail profile for wget
# Description: Retrieves files from the web
# This file is overwritten after every install/update
# Persistent local customizations
include /etc/firejail/wget.local
# Persistent global definitions
include /etc/firejail/globals.local

blacklist /tmp/.X11-unix

noblacklist ${HOME}/.wget-hsts
noblacklist ${HOME}/.wgetrc
## noblacklist ${HOME}/.config/
noblacklist /etc/wgetrc
noblacklist ~/Downloads
noblacklist /home/xe1phix/Downloads

whitelist /etc/wgetrc
whitelist ${HOME}/.wgetrc
whitelist ${HOME}/.wget-hsts

include /etc/firejail/disable-common.inc
include /etc/firejail/disable-programs.inc
include /etc/firejail/disable-devel.inc
include /etc/firejail/disable-interpreters.inc
include /etc/firejail/disable-passwdmgr.inc
include /etc/firejail/whitelist-common.inc
include /etc/firejail/whitelist-var-common.inc

caps.drop all
## netfilter
no3d
nodvd
nogroups
nonewprivs
nosound
notv
nou2f
novideo
protocol unix,inet
seccomp
## shell none


dns 185.121.177.177
dns 139.99.96.146


disable-mnt
# private-bin wget
private-dev
# private-etc resolv.conf,ca-certificates,ssl,pki,crypto-policies
# private-tmp

# memory-deny-write-execute
noexec ${HOME}
noexec /tmp
